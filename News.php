<?php

namespace loveitgroup\portalengine\basic\news;

use loveitgroup\portalengine\Module;

/**
 * News - класс модуля новостей
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class News extends Module
{
    /**
     * Количество новостей на страницу при просмотре списка
     * @var int
     */
    public $newsPerPage = 10;

    /**
     *
     * @var string
     */
    public $controllerNamespace = 'loveitgroup\portalengine\basic\news\controllers';

    /**
     * Правила роутинга
     * @var array
     */
    protected static $moduleUrlRules = [
        'news/view/<url:[a-z0-9-_]+>' => '/news/default/view',
        'news/<page:[0-9]+>'          => '/news/default/list',
        'news'                        => '/news/default/list',
    ];

    /**
     *
     * @var array
     */
    protected $defaultModels = [
        'Item'       => 'loveitgroup\portalengine\basic\news\models\Item',
        'ItemSearch' => 'loveitgroup\portalengine\basic\news\models\search\ItemSearch',
        'ItemQuery'  => 'loveitgroup\portalengine\basic\news\models\query\ItemQuery',
    ];

}
