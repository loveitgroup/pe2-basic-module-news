<?php

return [
    [
        'label'   => Yii::t('news.config', 'News'),
        'icon'    => 'fas fa-newspaper nav-icon',
        'url'     => ['/news/admin/item_admin/index'],
        'active'  => Yii::$app->controller->module->id == 'news',
        'visible' => Yii::$app->admin->can('manage_news'),
    ],
];

