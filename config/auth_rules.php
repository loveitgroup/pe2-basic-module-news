<?php

return [
    'title' => Yii::t('news.config', 'News'),
    'items' => [
        'manage_news' => Yii::t('news.config', 'Manage news'),
    ]
];
