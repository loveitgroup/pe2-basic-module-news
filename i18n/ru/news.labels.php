<?php 
 return array (
  'ID' => NULL,
  'Title' => 'Название',
  'Url' => NULL,
  'Short text' => 'Краткое описание',
  'Text' => 'Текст',
  'Meta Title' => NULL,
  'Meta Description' => NULL,
  'Meta Keywords' => NULL,
  'Publication date' => 'Дата публикации',
  'Status' => 'Статус',
  'Create datetime' => 'Добавлена',
  'Update datetime' => 'Обновлена',
  'Add news' => 'Добавить новость',
);