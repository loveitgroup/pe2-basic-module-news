<?php 
 return array (
  'Edit news {title}' => 'Редактирование новости {title}',
  'Main information' => 'Основная информация',
  'Meta tags' => 'Мета теги',
  'Additional information' => 'Дополнительная информация',
  'News' => 'Новости',
  'Add news' => 'Добавить новость',
  'Your data has been successfully saved!' => 'Ваши данные успешно сохранены!',
);