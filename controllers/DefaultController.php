<?php

namespace loveitgroup\portalengine\basic\news\controllers;

use loveitgroup\portalengine\FrontendController;
use Yii;
use yii\web\NotFoundHttpException;
 /**
 * DefaultController - класс контроллера для просмотра новостей
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class DefaultController extends FrontendController
{

    /**
     *
     * @return string
     */
    public function actionList()
    {
        $searchModel = $this->module->getModel('ItemSearch');
        $dataProvider = $searchModel->frontendSearch(Yii::$app->request->getQueryParams());

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    /**
     *
     * @param string $url
     * @return string
     */
    public function actionView($url)
    {
        $modelName = $this->module->getModel('Item', false);

        $model = $modelName::find()->url($url)->published()->one();

        if(!$model) {
            throw new NotFoundHttpException( Yii::t('news.frontend', 'News not found'));
        }
        return $this->render('view', [
            'model' => $model
        ]);
    }

}
