<?php

namespace aloveitgroup\portalengine\basic\news\controllers\admin;

use loveitgroup\portalengine\AdminController;
 /**
 * DefaultController - класс контроллера
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class DefaultController extends AdminController
{
    /**
     *
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
