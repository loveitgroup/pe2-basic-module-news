<?php

namespace loveitgroup\portalengine\basic\news\controllers\admin;

use loveitgroup\portalengine\AdminController;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

 /**
 * Item_adminController - контроллер для управления моделями Item
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class Item_adminController extends AdminController
{

    /**
     *@inheritdoc
     */
    public function beforeAction($event)
    {
        if(!parent::beforeAction($event)) {
            return false;
        }
        if(!Yii::$app->admin->can('manage_news')) {
            throw new ForbiddenHttpException(Yii::t('core.backend', 'You do not have sufficient permissions to access this page'));
        }
        return true;
    }

    /**
     * Просмотр моделей Item.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = $this->module->getModel('ItemSearch');
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Редактирование модели Item.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id = 0)
    {
        $post = Yii::$app->request->post();
        $modelName = $this->module->getModel('Item', false);
        $model = $modelName::findOne($id);

        $isNew = false;
        if (!$model) {
            $model = new $modelName;
            $model->status = $modelName::STATUS_DRAFT;
            $isNew = true;
        }
        $model->scenario = 'admin';

        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } elseif ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('adminSuccess', Yii::t('news.backend', 'Your data has been successfully saved!'));

            if ($isNew) {
                return $this->redirect(['edit', 'id' => $model->id]);
            } else {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Удаление модели Item.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $modelName = $this->module->getModel('Item', false);
        $model = $modelName::findOne($id);
        if($model) {
            $model->delete();
        }
        if(!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }
    }
}
