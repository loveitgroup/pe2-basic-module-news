<?php

namespace loveitgroup\portalengine\basic\news\models\search;

use loveitgroup\portalengine\basic\news\models\Item;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


 /**
 * ItemSearch - модель для поиска по `loveitgroup\portalengine\basic\news\models\Item`
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class ItemSearch extends Item
{
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'url', 'short_text', 'text', 'meta_title', 'meta_description', 'meta_keywords', 'publication_date', 'create_datetime', 'update_datetime'], 'safe'],
            [['create_datetime', 'update_datetime'], 'default', 'value' => NULL],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $className = Yii::$app->getModule('news')->getModel('Item', false);
        $query = $className::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'        => Yii::$app->controller->pageSize,
                'defaultPageSize' => Yii::$app->controller->pageSize,
                'forcePageParam'  => false
            ],
            'sort'       => ['defaultOrder' => ['update_datetime' => SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'publication_date' => $this->publication_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'create_datetime', $this->create_datetime])
            ->andFilterWhere(['like', 'update_datetime', $this->update_datetime]);

        return $dataProvider;
    }


    public function frontendSearch($params)
    {
        $module = Yii::$app->getModule('news');
        $className = $module->getModel('Item', false);
        $query = $className::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'        => $module->newsPerPage,
                'defaultPageSize' => $module->newsPerPage,
                'forcePageParam'  => false
            ],
            'sort'       => ['defaultOrder' => ['publication_date' => SORT_DESC]]
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
