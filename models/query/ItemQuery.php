<?php

namespace loveitgroup\portalengine\basic\news\models\query;

use loveitgroup\portalengine\ActiveQuery;
use Yii;

/**
 * ItemQuery - Scopes for loveitgroup\portalengine\basic\news\models\Item
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class ItemQuery extends ActiveQuery
{
    /**
     *
     * @return $this
     */
    public function url($url)
    {
        $this->andWhere(['url' => $url]);
        return $this;
    }

    /**
     *
     * @return $this
     */
    public function published()
    {
        $className = Yii::$app->getModule('news')->getModel('Item', false);
        $this->andWhere(['status' => $className::STATUS_PUBLISHED]);
        return $this;
    }

}
