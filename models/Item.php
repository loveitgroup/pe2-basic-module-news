<?php

namespace loveitgroup\portalengine\basic\news\models;

use loveitgroup\portalengine\ActiveRecord;
use loveitgroup\portalengine\basic\medialibrary\components\AjaxUploadImageBehavior;
use loveitgroup\portalengine\basic\shop\models\query\ItemQuery;
use loveitgroup\portalengine\behaviors\DatetimeBehavior;
use loveitgroup\portalengine\validators\CreateUrl;
use loveitgroup\portalengine\validators\SimpleSanitize;
use Yii;

/**
 * Item - Модель для таблицы pe2_news_item
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $short_text
 * @property string $text
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $publication_date
 * @property integer $status
 * @property string $create_datetime
 * @property string $update_datetime
 */
class Item extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_item}}';
    }

    /**
     * @inheritdoc
     * @return ItemQuery
     */
    public static function find()
    {
        $className = Yii::$app->getModule('news')->getModel('ItemQuery', false);
        return new $className(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //title,
            [['title'], 'filter', 'filter' => 'trim'],
            [['title'], SimpleSanitize::className()],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            //url,
            [['url'], CreateUrl::className()],
            [['url'], 'filter', 'filter' => 'trim'],
            [['url'], SimpleSanitize::className()],
            [['url'], 'string', 'max' => 255],
            //short_text,
            [['short_text'], 'filter', 'filter' => 'trim'],
            [['short_text'], 'string'],
            //text,
            [['text'], 'filter', 'filter' => 'trim'],
            [['text'], 'string'],
            //meta_title,
            [['meta_title'], 'filter', 'filter' => 'trim'],
            [['meta_title'], SimpleSanitize::className()],
            [['meta_title'], 'string', 'max' => 255],
            //meta_description,
            [['meta_description'], 'filter', 'filter' => 'trim'],
            [['meta_description'], SimpleSanitize::className()],
            [['meta_description'], 'string', 'max' => 255],
            //meta_keywords,
            [['meta_keywords'], 'filter', 'filter' => 'trim'],
            [['meta_keywords'], SimpleSanitize::className()],
            [['meta_keywords'], 'string', 'max' => 255],
            //publication_date,
            [['publication_date'], 'default', 'value' => NULL],
            [['publication_date'], 'date', 'format' => 'php:Y-m-d'],
            [['publication_date'], 'required'],
            //status,
            [['status'], 'required', 'on' => 'admin'],
            [['status'], 'in', 'range' => array_keys(self::getStatusTextList()), 'on' => 'admin'],
            //create_datetime,
            //update_datetime
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('news.labels', 'ID'),
            'title'            => Yii::t('news.labels', 'Title'),
            'url'              => Yii::t('news.labels', 'Url'),
            'short_text'       => Yii::t('news.labels', 'Short text'),
            'text'             => Yii::t('news.labels', 'Text'),
            'meta_title'       => Yii::t('news.labels', 'Meta Title'),
            'meta_description' => Yii::t('news.labels', 'Meta Description'),
            'meta_keywords'    => Yii::t('news.labels', 'Meta Keywords'),
            'publication_date' => Yii::t('news.labels', 'Publication date'),
            'status'           => Yii::t('news.labels', 'Status'),
            'create_datetime'  => Yii::t('news.labels', 'Create datetime'),
            'update_datetime'  => Yii::t('news.labels', 'Update datetime'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'datetime'        => [
                'class' => DatetimeBehavior::className(),
            ],
        ];
    }

}
