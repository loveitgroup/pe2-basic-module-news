<?php

namespace loveitgroup\portalengine\basic\news\widgets;

use Yii;
use yii\base\Widget;

/**
 * LastNewsWidget - виджет для отображения последних новостей
 * @author Rakovich Vladimir <rak.kture@gmail.com>
 * @since 2.0.1
 */
class LastNewsWidget extends Widget
{

    /**
     *
     * @var int
     */
    public $count = 5;

    /**
     *
     */
    public function run()
    {
        if($this->count < 1) {
            return;
        }
        $modelName = Yii::$app->getModule('news')->getModel('Item', false);

        $models = $modelName::find()
                ->published()
                ->limit($this->count)
                ->orderBy('publication_date DESC')
                ->all();

        if(!count($models)) {
            return;
        }

        return $this->render('last_news', [
            'models' => $models
        ]);

    }

}
