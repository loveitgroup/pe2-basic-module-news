<?php

use yii\helpers\Html;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?= Html::a(Yii::t('news.frontend', 'News'), ['/news/default/list']) ?>
        </h3>
    </div>
    <div class="panel-body">
        <?php foreach ($models as $model): ?>
            <div class="row">
                <div class="col-12">
                    <b>
                        <?= Html::a($model->title, ['/news/default/view', 'url' => $model->url]) ?>
                    </b>
                    <br>
                    <?= $model->short_text ?>
                    <br>
                    <?= Yii::$app->formatter->asDate($model->publication_date, 'medium')?>
                </div>
            </div>
            <hr>
        <?php endforeach; ?>
    </div>
</div>

