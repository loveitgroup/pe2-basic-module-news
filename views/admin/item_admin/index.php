<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use loveitgroup\portalengine\grid;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var loveitgroup\portalengine\basic\news\models\search\ItemSearch $searchModel
 */
$this->title = Yii::t('news.backend', 'News');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;

$className = Yii::$app->getModule('news')->getModel('Item', false);
?>
<div class="card">
    <div class="card-body">
        <div class="item-index">
            <div class="row">
                <div class="col-12">
                    <div class="float-right">
                        <?= Html::a('<span class="fas fa-plus-circle"></span> ' . Yii::t('news.backend', 'Add news'), ['edit'], ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php Pjax::begin(); ?>
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel'  => $searchModel,
                        'columns'      => [
                            'id',
                            'title',
                            'url',
                            [
                                'attribute' => 'status',
                                'value' => function ($model) use($className) {
                                    return $className::getStatusTextList($model->status);
                                },
                                'filter' => $className::getStatusTextList()
                            ],
                            [
                                'class'     => grid\DateColumn::className(),
                                'attribute' => 'publication_date',
                                'dateType' => grid\DateColumn::TYPE_DATE
                            ],
                            [
                                'class' => grid\MetadataColumn::className(),
                                'fields' => [
                                    'meta_title',
                                    'meta_description',
                                    'meta_keywords',
                                    'short_text',
                                    'text',
                                ]
                            ],
                            [
                                'class'     => grid\DateColumn::className(),
                                'attribute' => 'create_datetime',
                            ],
                            [
                                'class'     => grid\DateColumn::className(),
                                'attribute' => 'update_datetime',
                            ],
                            ['class' => grid\ActionColumn::className()],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
