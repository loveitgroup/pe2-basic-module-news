<?php

use loveitgroup\portalengine\twbs\ActiveForm;
use yii\jui\DatePicker;

/**
 * @var $this yii\web\View
 * @var $model loveitgroup\portalengine\basic\news\models\Item
 * @var $form yii\widgets\ActiveForm
 */


$this->title = Yii::t('news.backend', 'Edit news {title}', ['title' => $model->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('news.labels', 'Add news'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;

$className = Yii::$app->getModule('news')->getModel('Item', false);
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php if($model->isNewRecord): ?>
            <div class="col-12 col-md-8">
                <div class="card card-primary card-outline ">
                    <div class="card-header">
                        <h3 class="card-title">
                            <?= Yii::t('news.backend', 'Main information') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <?= $form->errorSummary($model); ?>

                        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'publication_date')->widget(DatePicker::className(), [
                            'options'       => ['class' => 'form-control'],
                            'dateFormat'    => 'php:Y-m-d',
                            'clientOptions' => [

                            ]
                        ]) ?>
                    </div>
                    <div class="card-footer">
                        <?= $form->submitButton($model) ?>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="col-12 col-md-8">
                <div class="card card-primary card-outline ">
                    <div class="card-header">
                        <h3 class="card-title">
                            <?= Yii::t('news.backend', 'Main information') ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'short_text')->wysiwyg() ?>

                        <?= $form->field($model, 'text')->wysiwyg() ?>

                    </div>
                <div class="card-footer">
                    <?= $form->submitButton($model) ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">
                                <?= Yii::t('news.backend', 'Meta tags') ?>
                            </h3>
                        </div>
                        <div class="card-body">
                            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">
                                <?= Yii::t('news.backend', 'Additional information') ?>
                            </h3>
                        </div>
                        <div class="card-body">
                            <?= $form->field($model, 'publication_date')->widget(DatePicker::className(), [
                                'options'       => ['class' => 'form-control'],
                                'dateFormat'    => 'php:Y-m-d',
                                'clientOptions' => [

                                ]
                            ]) ?>

                            <?= $form->field($model, 'status')->dropDownList($className::getStatusTextList()) ?>
                            <?php  if(!$model->isNewRecord): ?>
                                <?= $form->field($model, 'create_datetime')->plainDatetime() ?>

                                <?= $form->field($model, 'update_datetime')->plainDatetime() ?>
                            <?php  endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>