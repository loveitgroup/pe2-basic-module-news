<?php

use yii\helpers\Html;

?>

<div class="row">
    <div class="col-12">
        <h3>
            <?= Html::a($model->title, ['/news/default/view', 'url' => $model->url])?>
        </h3>
        <div class="row">
            <div class="col-12">
                <?= Yii::t('news.frontend', 'Publication date:')?>
                <b>
                    <?= Yii::$app->formatter->asDate($model->publication_date, 'medium')?>
                </b>
                <br>
                <?= $model->short_text ?>
            </div>
        </div>
    </div>
</div>
<hr>