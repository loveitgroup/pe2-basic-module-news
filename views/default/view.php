<?php

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('news.frontend', 'News'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>
    <?= $this->title?>
</h1>
<div class="row">
    <div class="col-12">
        <?= Yii::t('news.frontend', 'Publication date:')?>
        <b>
            <?= Yii::$app->formatter->asDate($model->publication_date, 'medium')?>
        </b>
        <br>
        <?= $model->text ?>
    </div>
</div>