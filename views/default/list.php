<?php

use yii\widgets\ListView;

$this->title = Yii::t('news.frontend', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-12">
        <h1><?= $this->title ?></h1>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView'     => '_single_item',
            'summary'      => false,
            'options'      => [
                'tag'   => 'div',
                'class' => 'items',
            ],
            'itemOptions'  => [
                'tag' => 'div',
            ]
        ])
        ?>
    </div>
</div>
